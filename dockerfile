FROM nginx

MAINTAINER Hutta

RUN apt-get -y update
RUN apt-get install -y --force-yes php5-fpm supervisor

# Copy supervisord.conf for start service php and nginx
COPY supervisord.conf /etc/supervisord.conf

# Config php and nginx
RUN echo 'cgi.fix_pathinfo=0' >> /etc/php5/fpm/php.ini
COPY www.conf /etc/php5/fpm/pool.d/www.conf
COPY default.conf /etc/nginx/conf.d/default.conf

CMD ["/usr/bin/supervisord"]
